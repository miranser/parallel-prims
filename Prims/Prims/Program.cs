﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using MPI;
using Environment = MPI.Environment;

namespace Prims
{
    [Serializable]
    internal class Edge
    {
        public int fromNode;
        public int toNode;
        public int weight;

        public Edge(int fromnode, int tonode, int weight)
        {
            fromNode = fromnode;
            toNode = tonode;
            this.weight = weight;
        }

        public override string ToString()
        {
            return fromNode + 1 + "<-------" + weight + "------->" + (toNode + 1);
        }
    }
    internal class Program
    {
        private const int inf = int.MaxValue;

        private static int[,] GetMatrix()
        {
            var lines = File.ReadAllLines("Matrix.txt");
            List<Edge> digitsd = new List<Edge>();
            var matrix = new int[9, 9];
            /*for (var j = 0; j < 9; j++)
            {
                for (var k = 0; k < 9; k++)
                {
                    matrix[j, k] = inf;
                }
            }*/
            var i = 0;
            foreach (var line in lines)
            {
                var digits = line.Split(',');
                digitsd.Add(new Edge(int.Parse(digits[0]), int.Parse(digits[1]), int.Parse(digits[2])));
                i++;
            }
            var n = digitsd.Max(a => Math.Max(a.fromNode, a.toNode));
            for (int j = 0; j < n; j++)
            {
                for (int k = 0; k < n; k++)
                {
                    matrix[j, k] = inf;
                }
            }
            foreach (var tuple in digitsd)
            {
                matrix[tuple.fromNode - 1, tuple.toNode - 1] = tuple.weight;
                matrix[tuple.toNode - 1, tuple.fromNode - 1] = tuple.weight;
            }
            return matrix;
        }

        private static void PrintMatrix(int[,] matrix)
        {
            for (var i = 0; i < matrix.GetLength(0); i++)
            {
                for (var j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] == inf) Console.Write(0 + " ");
                    else Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        private static List<List<Edge>> GetD(int[,] G)
        {
            var D = new List<List<Edge>>();
            for (var i = 0; i < G.GetLength(0); i++)
            {
                D.Add(new List<Edge>());
                for (var j = 0; j < G.GetLength(1); j++)
                {
                    if (G[i, j] != inf) D[i].Add(new Edge(i, j, G[i, j]));
                }
            }
            return D;
        }

        private static List<List<Edge>>[] SplitToDPart(Communicator comm, List<List<Edge>> D)
        {
            var DParts = new List<List<Edge>>[comm.Size];
            for (var i = 0; i < comm.Size; i++)
            {
                DParts[i] = new List<List<Edge>>();
                for (var j = 0; j < D.Count/comm.Size; j++)
                {
                    DParts[i].Add(D[i*comm.Size + j]);
                }
            }
            return DParts;
        }
        private static void Main(string[] args)
        {
            using (new Environment(ref args))
            {
                var comm = Communicator.world;
                var G = GetMatrix();        //adjacency matrix. takes from a file every srting is an edge in f,t,w format ,where f and t - vertices, w - weight of an edge 
                var Vt = new List<int>();   //vertices of current MST
                var minEdges = new List<Edge>();    //list of min edges of current process
                var MST = new List<Edge>();     //output MST
                List<List<Edge>> Dpart;     //part of adjacency list
                var min = new Edge(0, 0, inf);      //minimal weighted edge of current process
                var NOfNodes = G.GetLength(0);      //number of vertices
                var D = GetD(G);        //adjacency list

                var sw = new Stopwatch();
                sw.Start();
                Vt.Add(0);          //vertex from algorythm starts
                comm.Broadcast(ref Vt, 0);  //broadcasting vertices of MST to all pocesses
                if (comm.Rank == 0)
                {
                    PrintMatrix(G);
                    var Dparts = SplitToDPart(comm, D);     // splitting adjacency list on parts  
                    Dpart = comm.Scatter(Dparts);       //sending parts to all processes
                }
                else
                {
                    Dpart = comm.Scatter<List<List<Edge>>>(0); //--//--
                }
                while (Vt.Count != NOfNodes)
                {
                    for (var i = 0; i < Dpart.Count; i++)
                    {
                        min = new Edge(0, 0, inf);
                        if (Vt.Contains(i + comm.Size*comm.Rank))
                        {
                            foreach (var g in Dpart[i])
                            {
                                if (g.weight < min.weight && !Vt.Contains(g.toNode))
                                {
                                    min = new Edge(g.fromNode, g.toNode, g.weight);
                                }
                            }
                        }
                        if (!Vt.Contains(min.toNode))
                        {
                            minEdges.Add(min);
                        }
                    }
                    var tminEdges = comm.Gather(minEdges, 0);   //gathering minimal edges from all processes
                    if (comm.Rank == 0)
                    {
                        for (var i = 0; i < tminEdges.Length; i++)
                        {
                            for (var j = 0; j < tminEdges[i].Count; j++)
                            {
                                if (!Vt.Contains(tminEdges[i][j].toNode))
                                {
                                    Vt.Add(tminEdges[i][j].toNode);
                                    MST.Add(tminEdges[i][j]);
                                }
                            }
                        }
                    }
                    comm.Broadcast(ref Vt, 0);       //broacasting changed adjacency list 
                }
                sw.Stop();
                var time = sw.Elapsed; 
                if (comm.Rank == 0)
                {
                    Console.WriteLine("\n\n MST vertices: ");
                    foreach (var i in Vt)
                    {
                        Console.Write(i + 1 + " ");
                    }
                    
                    Console.WriteLine("\n\n MST edges: ");
                    for (var i = 0; i < MST.Count; i++)
                    {
                        Console.WriteLine(MST[i].ToString());
                    }

                   // Console.WriteLine("\n\n G total weight: "+D.Sum(a=>a.w));
                    Console.WriteLine("\n\n MST total weight: "+MST.Sum(a=>a.weight));
                    Console.WriteLine("\n\n total time: " + time.Milliseconds + "ms");
                }
            }

            //*******************************SEQUENTiAL Algorythm*****************************************
            /*var G = GetMatrix();
                PrintMatrix(G);
                
                List<int>V = new List<int>();
                List<int>Vt = new List<int>();
                List<int> f = new List<int>();
                List<int> t = new List<int>();
                List<List<Edge>>D = new List<List<Edge>>();
                V.Add(0);

                for (int i = 0; i < G.GetLength(0); i++)
                {
                    D.Add( new List<Edge>());
                    for (int j = 0; j < G.GetLength(1); j++)
                    {
                        if(G[i,j]!=inf)D[i].Add(new Edge(i,j,G[i,j]));

                    }
                }

                var sw = new Stopwatch();
                sw.Start();
                Vt.Add(0);

                while (Vt.Count!=D.Count)
                {
                    Edge min=new Edge(0,0,inf);

                    for (int i = 0; i < D.Count; i++)
                    {
                        if (Vt.Contains(i))
                        {
                            foreach (var g in D[i])
                            {
                                if (g.weight < min.weight && !Vt.Contains(g.toNode))
                                {
                                    min = new Edge(g.fromNode,g.toNode,g.weight);
                                }
                            }
                        }
                        if (!Vt.Contains(min.toNode))
                        {
                            f.Add(i);
                            t.Add(min.toNode);
                            Vt.Add(min.toNode);
                        }
                    }
                    
                }
            sw.Stop();
            var time = sw.Elapsed;

                foreach (var i in Vt)
                {
                    Console.Write((i+1)+" ");
                }

                for (int i = 0; i < f.Count; i++)
                {
                    Console.WriteLine("{0}-{1}",(f[i]+1),(t[i]+1));
                }
            Console.WriteLine("time elapsed "+ time.Milliseconds+"ms");
            Console.ReadLine();
            */
        }
    }
}